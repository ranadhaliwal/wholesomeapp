FROM java:8
ADD ./target/*.jar wholesomeapp.jar
RUN bash -c "touch wholesomeapp.jar"
ENTRYPOINT ["java","-jar","wholesomeapp.jar", ">", "wsapp.log"]
