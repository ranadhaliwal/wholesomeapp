package com.wholesomeapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by allapaj on 4/15/17.
 */
@RestController
@RequestMapping("/user")
public class MainController {

    @Autowired
    UserRepository repo;

    @RequestMapping("/{id}")
    public User getUser(@PathVariable Long id) {

        return repo.findById(id);
    }
}
