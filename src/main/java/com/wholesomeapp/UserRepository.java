package com.wholesomeapp;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

/**
 * Created by allapaj on 4/15/17.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findByLastName(String lastName);

    User findById(Long id);
}
